from random import randint


CAR_SPECS = {
    'ferrary': {"max_speed": 340, "drag_coef": 0.324, "time_to_max": 26},
    'bugatti': {"max_speed": 407, "drag_coef": 0.39, "time_to_max": 32},
    'toyota': {"max_speed": 180, "drag_coef": 0.25, "time_to_max": 40},
    'lada': {"max_speed": 180, "drag_coef": 0.32, "time_to_max": 56},
    'sx4': {"max_speed": 180, "drag_coef": 0.33, "time_to_max": 44},
}


class Car:
    def __init__(self, model, max_speed, drag_coef, time_to_max):
        self.model = model
        self.max_speed = max_speed
        self.drag_coef = drag_coef
        self.time_to_max = time_to_max

    def round(self, distance, weather):
        wind_speed = weather.get_wind_speed
        competitor_time = 0

        for dist in range(distance):
            if competitor_time == 0:
                speed = 1
            else:
                speed = competitor_time / self.time_to_max * self.max_speed
                if speed > wind_speed:
                    speed -= self.drag_coef * wind_speed
            competitor_time += float(1) / speed
        print("Car <%s> result: %f" % (self.model, competitor_time))


class Weather:
    def __init__(self, wind_speed):
        self._wind_speed = wind_speed

    @property
    def get_wind_speed(self):
        return randint(0, self._wind_speed)


class Contest:
    _count = 0

    def __new__(cls, *args, **kwargs):
        if cls._count == 0:
            cls._count += 1
            return super(Contest, cls).__new__(cls)
        else:
            raise RuntimeError("Class Contest can have only one instance.")

    def __init__(self, distance):
        self.distance = distance

    def start(self, car, weather):
        car.round(self.distance, weather)


def main():
    contest = Contest(1000)
    weather = Weather(20)
    for car in CAR_SPECS:
        model = CAR_SPECS[car]
        new_car = Car(car, model["max_speed"], model["drag_coef"], model["time_to_max"])
        contest.start(new_car, weather)


if __name__ == "__main__":
    main()
